const problem4=require("../callback4.cjs");
function callback(error,boardDetails,listDetails,cardDetails){
    if(error){
        console.error(error);
    }else{
        // boardDetails=Object.fromEntries(boardDetails)
        console.log(boardDetails);
        // listDetails= Object.fromEntries(listDetails)
        console.log(Object.fromEntries(listDetails));
        // cardDetails=Object.fromEntries(cardDetails)
        console.log(Object.fromEntries(cardDetails));
    }
}
problem4('mcu453ed',callback);