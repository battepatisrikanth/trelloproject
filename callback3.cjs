/* 
	Problem 3:
    Write a function that will return all cards that belong to a particular list
    based on the listID that is passed to it from the given data in cards.json.
    Then pass control back to the code that called it by using a callback function.
*/

const fs=require('fs');
const path=require('path');
const filePath=path.resolve("cards.json");
function cardsInfo(listID,callback){
    // const resultArray=[];
    setTimeout(()=>{
        fs.readFile(filePath,'utf-8',(error,data)=>{
            if(error){
                callback(error);
            }else{
                data=JSON.parse(data);
                dataArray=Object.entries(data);
                // resultArray.push(data[0])
                // console.log(data);
                let result=dataArray.filter(function (element){
                    return (element[0]===listID)
   
                    })
                    // console.log(typeof(data));
                callback(null,result);
                // console.log(result);
            }   
        })   
    },0);
}


module.exports=cardsInfo;