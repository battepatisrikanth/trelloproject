const boardInfo=require("./callback1.cjs");
const cardsInfo=require("./callback3.cjs");
const listInfo=require("./callback2.cjs");

function problem6(boardID,callback){
    setTimeout(()=>{
    boardInfo(boardID,returnThanosBoardInfo);
    function returnThanosBoardInfo(error,data){
        if(error){
            console.error(error);
        }
        else{
            const boardDetails=data;
            listInfo(boardID,listForThanosBoard);
            function listForThanosBoard(error,data){
                if(error){
                    console.error(error);
                }else{
                    const listDetails=data;
                    // console.log(listDetails);
                    const mindListCards=listDetails[0][1];
                    // console.log(mindListCards);
                    cardsInfo(mindListCards[0].id,cardsForMindList);
                    function cardsForMindList(error,data){
                        if(error){
                            console.error(error);
                        }else{
                            const cardDetails=data;
                            for(let index=0;index<mindListCards.length;index++){
                                cardsInfo(mindListCards[index].id,(error,data)=>{
                                    if(error){
                                     console.error(error);
                                    }else{
                                        console.log(Object.fromEntries(data));
                                    }
                                });
                            }
                            // console.log(cardDetails);
                            callback(null,boardDetails,listDetails,cardDetails);
                        }
                    }

                
                }
            }
        }
    }
},0)
}

module.exports=problem6;